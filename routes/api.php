<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MicroServices\MsAutoCompleteField;
use App\Http\Controllers\MicroServices\MsMgrProductStructureType;
use App\Models\PRDRPRODUCTTYPESTRUCTURE;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('fs.')->prefix('fs')->group(function () {
    Route::name('autocomplete.')->prefix('autocomplete')->group(function () {
        Route::get('productType', [MsAutoCompleteField::class, 'productType'])->name('productType');
        Route::get('materialType', [MsAutoCompleteField::class, 'materialType'])->name('materialType');
        Route::get('productStructureType', [MsAutoCompleteField::class, 'productStructureType'])->name('productStructureType');
        Route::get('materialUnitType', [MsAutoCompleteField::class, 'materialUnitType'])->name('materialUnitType');
    });
});

Route::name('mgr.')->prefix('mgr')->group(function () {
    Route::name('productStructureType.')->prefix('productStructureType')->group(function () {
        Route::get('getTableIndex', [MsMgrProductStructureType::class, 'getTableIndex'])->name('getTableIndex');
        Route::post('storeNewData', [MsMgrProductStructureType::class, 'storeNewData'])->name('storeNewData');
        Route::delete('deleteData', [MsMgrProductStructureType::class, 'deleteData'])->name('deleteData');
    });
});


//only for testing in python
Route::name('product.')->prefix('product')->group(function () {
    Route::get('get_all_product', function () {
        $data = PRDRPRODUCTTYPESTRUCTURE::all();
        return $data;
    });
});
