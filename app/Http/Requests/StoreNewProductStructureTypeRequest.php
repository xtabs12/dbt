<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

/**
 * Class StoreNewProductStructureTypeRequest
 * @package App\Http\Requests
 * @property mixed idNode
 * @property mixed productType
 * @property mixed materialType
 * @property mixed nodeParent
 * @property mixed name
 * @property mixed total
 * @property mixed unitType
 */
class StoreNewProductStructureTypeRequest extends FormRequest
{
    /**
     * @var mixed
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {
        return [
            'idNode' => 'required|string|max:255|unique:PRD_R_PRODUCTTYPESTRUCTURE,IDNODE',
            'productType' => 'required|string|exists:PRD_R_PRODUCTTYPE,CODE',
            'materialType' => 'required|string|exists:MAT_R_MATERIALTYPE,CODE',
            'nodeParent' => 'nullable|string|exists:PRD_R_PRODUCTTYPESTRUCTURE,IDNODE',
            'name' => 'nullable|string',
            'total' => 'integer',
            'unitType' => 'required|string|exists:GEN_R_MATERIALUNIT,ID'
        ];
    }

    public function response(array $errors): object
    {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever els
        return response()->json(['Result' => 'ERROR', 'Message' => 'error'], 402); // i wanted the Message to be a string
    }

//    public function failedValidation(Validator $validator)
//    {
//        return response()->json(['Result' => 'ERROR', 'Message' => 'error'],402); // i wanted the Message to be a string
//    }
//    public function failedValidation(Validator $validator)
//    {
//        $vdata = [];
//        $response = [];
//        $response['data'] = [];
//        $response['status'] = 0;
//        $response['message'] = trans('messages.validation_errors');
////        $response['errors'] = $validator->errors();
//        $response['errors'] = [];
//        $totalErrors = count($validator->errors());
//        for ($i = 0; $i < $totalErrors; $i++) {
//            array_push($response['errors'], $validator->errors()[$i]);
//        }
//
//        throw new HttpResponseException(response()->json($response, 422));
//    }
}
