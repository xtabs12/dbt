<?php

namespace App\Http\Controllers\MicroServices;

use App\Http\Requests\StoreNewProductStructureTypeRequest as REQA;
use App\Http\Requests\DeleteProductStructureType as REQB;
use App\Models\PRDRPRODUCTTYPESTRUCTURE;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MsMgrProductStructureType extends Controller
{
    public function getTableIndex(): object
    {
        $dbo = PRDRPRODUCTTYPESTRUCTURE::all();
        $dataListing = $dbo->map(function ($db) {
            return [
                'id' => $db->IDNODE,
                'name' => $db->NAME,
                'productType' => $db->productType ? '(' . $db->productType->CODE . ') ' . $db->productType->NAME : null,
                'materialType' => $db->materialType ? '(' . $db->materialType->CODE . ') ' . $db->materialType->NAME : null,
                'parentNode' => $db->NODEPARENT,
                'total' => $db->QUANTITY,
                'units' => $db->unitType ? $db->unitType->NAME : null,
                'actions' => 'hello'
            ];
        });
        $dataHeaders = null;
        return response()->json($dataListing, 200);
    }

    public function storeNewData(REQA $request): object
    {
        try {
            $oToRay = array(
                'IDNODE' => $request->idNode,
                'PRODUCTTYPECODE' => $request->productType,
                'MATERIALTYPECODE' => $request->materialType,
                'NODEPARENT' => $request->nodeParent,
                'NAME' => $request->name,
                'QUANTITY' => $request->total,
                'UNIT' => $request->unitType,
            );
            PRDRPRODUCTTYPESTRUCTURE::create($oToRay);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('200: input data success', 200);
    }

    public function deleteData(REQB $request): object
    {
        $data = [];
        $dbo = PRDRPRODUCTTYPESTRUCTURE::where('IDNODE', $request->id)->first();
        if ($dbo == null) {
            return response()->json('data not found', 404);
        }
        $dbo->delete();
        return response()->json($data, 200);
    }
}
