<?php

namespace App\Http\Controllers\MicroServices;

use App\Models\GENRMATERIALUNIT;
use App\Models\MATRMATERIALTYPE;
use App\Models\PRDRPRODUCTTYPE;
use App\Models\PRDRPRODUCTTYPESTRUCTURE;
use App\Http\Controllers\Controller;

class MsAutoCompleteField extends Controller
{
    public function productType(): object
    {
        $dbo = PRDRPRODUCTTYPE::all();
        $data = $dbo->map(function ($db) {
            return ['text' => '(' . $db->CODE . ') ' . $db->NAME, 'value' => $db->CODE];
        });
        $tdbo = count($dbo);
        return response()->json($data, 200);
    }

    public function materialType(): object
    {
        $dbo = MATRMATERIALTYPE::all();
        $data = $dbo->map(function ($db) {
            return ['text' => '(' . $db->CODE . ') ' . $db->NAME, 'value' => $db->CODE];
        });
        return response()->json($data, 200);
    }

    public function productStructureType(): object
    {
        $dbo = PRDRPRODUCTTYPESTRUCTURE::all();
        $data = $dbo->map(function ($db) {
            return ['text' => '(' . $db->IDNODE . ') ' . $db->NAME, 'value' => $db->IDNODE];
        });
        return response()->json($data, 200);
    }

    public function materialUnitType(): object
    {

        $dbo = GENRMATERIALUNIT::where('BASE', null)->get();
        $data = $dbo->map(function ($db) {
            return ['text' => $db->NAME, 'value' => $db->ID];
        });
        return response()->json($data, 200);
    }
}
