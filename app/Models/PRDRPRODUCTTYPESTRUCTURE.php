<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PRDRPRODUCTTYPESTRUCTURE extends Model
{
    use HasFactory;

    protected $table = 'PRD_R_PRODUCTTYPESTRUCTURE';
    protected $primaryKey = "IDNODE";
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = ['IDNODE','PRODUCTTYPECODE','MATERIALTYPECODE','NODEPARENT','NAME','QUANTITY','UNIT'];

    public function parents(): HasOne
    {
        return $this->hasOne(PRDRPRODUCTTYPESTRUCTURE::class, 'IDNODE', 'NODEPARENT');
    }

    public function family(): HasMany
    {
        return $this->hasMany(PRDRPRODUCTTYPESTRUCTURE::class, 'IDNODE', 'NODEPARENT');
    }

    public function productType(): HasOne
    {
        return $this->hasOne(PRDRPRODUCTTYPE::class, 'CODE', 'PRODUCTTYPECODE');
    }

    public function materialType(): HasOne
    {
        return $this->hasOne(MATRMATERIALTYPE::class, 'CODE', 'MATERIALTYPECODE');
    }

    public function unitType(): HasOne
    {
        return $this->hasOne(GENRMATERIALUNIT::class, 'ID', 'UNIT');
    }
}
