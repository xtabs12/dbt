<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tripan extends Model
{
    use HasFactory;

    protected $table = 'PRD_R_PRODUCTTYPESTRUCTURE';

//    public function family(){
//        return $this->hasMany()
//    }

    public function parents()
    {
        return $this->hasOne(Tripan::class, 'IDNODE', 'NODEPARENT');
    }
}
