<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PRDRPRODUCTTYPE extends Model
{
    use HasFactory;
    protected $table = 'PRD_R_PRODUCTTYPE';
}
