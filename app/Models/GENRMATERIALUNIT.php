<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GENRMATERIALUNIT extends Model
{
    use HasFactory;

    protected $table = 'GEN_R_MATERIALUNIT';
}
