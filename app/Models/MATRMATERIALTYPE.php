<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MATRMATERIALTYPE extends Model
{
    use HasFactory;

    protected $table = 'MAT_R_MATERIALTYPE';
}
