@extends('layouts.core')
@section('content')
    <v-container>
        <v-btn color="primary" dark @click="isFormDialogOpen=true">ADD</v-btn>
        <template>
            <v-data-table
                :headers="apiData.table.productStructureType.headers"
                :items="apiData.table.productStructureType.listing"
                :loading="apiLoading.productStructureTypeTable"
                :items-per-page="5"
                class="elevation-1">
                <template v-slot:item.actions="{item}" min-width="200">
                    <v-btn @click="updateProductStructureType(item)" class="p-0" max-width="25px" height="25px" dense color="green">
                        <v-icon>mdi-circle-edit-outline</v-icon>
                    </v-btn>
                    <v-btn @click="deleteProductStructureType(item)" max-width="25px" height="25px" dense
                           color="error">
                        <v-icon>mdi-trash-can-outline</v-icon>
                    </v-btn>
                </template>
            </v-data-table>
        </template>
        <template>
            <v-dialog v-model="isFormDialogOpen">
                <v-card>
                    <v-card-title class="headline grey lighten-2">
                        Add New Data
                    </v-card-title>

                    <v-card-text class="p-5">
                        <v-text-field label="ID Node"
                                      class="mb-2"
                                      v-model="form.idNode"
                                      :error="!!formClientError.idNode"
                                      :error-messages="formClientError.idNode"
                                      outlined dense></v-text-field>
                        <v-autocomplete label="Tipe Produk"
                                        class="mb-2"
                                        v-model="form.productType"
                                        :loading="apiLoading.productTypeAutoComplete"
                                        :items="apiData.autoComplete.productType"
                                        :error="!!formClientError.productType"
                                        :error-messages="formClientError.productType"
                                        placeholder="pilih tipe produk"
                                        outlined dense></v-autocomplete>
                        <v-autocomplete label="Tipe Material"
                                        class="mb-2"
                                        v-model="form.materialType"
                                        :loading="apiLoading.materialTypeAutoComplete"
                                        :error="!!formClientError.materialType"
                                        :error-messages="formClientError.materialType"
                                        outlined dense placeholder="pilih tipe materials"
                                        :items="apiData.autoComplete.materialType"></v-autocomplete>
                        <v-autocomplete label="Node Parent"
                                        class="mb-2"
                                        v-model="form.nodeParent"
                                        :loading="apiLoading.nodeParentAutoComplete"
                                        :error="!!formClientError.nodeParent"
                                        :error-messages="formClientError.nodeParent"
                                        outlined dense placeholder="pilih node parent"
                                        :items="apiData.autoComplete.nodeParent"></v-autocomplete>
                        <v-text-field label="Nama"
                                      class="mb-2"
                                      v-model="form.name"
                                      :error="!!formClientError.name"
                                      :error-messages="formClientError.name"
                                      outlined dense></v-text-field>
                        <v-text-field label="Jumlah"
                                      class="mb-2"
                                      v-model="form.total"
                                      :error="!!formClientError.total"
                                      :error-messages="formClientError.total"
                                      outlined dense></v-text-field>
                        <v-autocomplete label="Unit"
                                        class="mb-2"
                                        v-model="form.unitType"
                                        :loading="apiLoading.materialUnitTypeAutoComplete"
                                        :error="!!formClientError.unitType"
                                        :error-messages="formClientError.unitType"
                                        :items="apiData.autoComplete.materialUnitType"
                                        placeholder="units"
                                        outlined dense></v-autocomplete>
                    </v-card-text>
                    <v-divider></v-divider>
                    <v-card-actions>
                        <v-spacer></v-spacer>
                        <v-btn
                            color="primary"
                            text
                            @click="postNewProductStructureType()"
                        >
                            Submit
                        </v-btn>
                        <v-btn
                            color="error"
                            text
                            @click="isFormDialogOpen = false"
                        >
                            Close
                        </v-btn>
                    </v-card-actions>
                </v-card>
            </v-dialog>
        </template>
        <v-snackbar centered v-model="snackbar.isDeleteCautionSnackbarOpened">
            @verbatim
                are you sure want to delete {{selectedToDelete.id}}
            @endverbatim
            <template v-slot:action="{ attrs }">
                <v-btn color="error" v-bind="attrs" dark
                       @click="commitDeleteProductStructureType">
                    Yes
                </v-btn>
                <v-btn text dark v-bind="attrs"
                       @click="cancelDeleteProductStructureType">
                    No
                </v-btn>
            </template>
        </v-snackbar>
    </v-container>
@endsection

@push('script')
    <script>
        let lAMixingPage = {
            data: () => {
                return {
                    aed: 'hello',
                    snackbar: {
                        isDeleteCautionSnackbarOpened: false,
                    },
                    selectedToDelete: {},
                    isFormDialogOpen: false,
                    urlData: {
                        productTypeAutoCompleteData: @json(route('fs.autocomplete.productType')),
                        materialTypeAutoCompleteData: @json(route('fs.autocomplete.materialType')),
                        productStructureTypeAutoCompleteData: @json(route('fs.autocomplete.productStructureType')),
                        materialUnitTypeAutoCompleteData: @json(route('fs.autocomplete.materialUnitType')),
                        productStructureTypeTableData:@json(route('mgr.productStructureType.getTableIndex')),
                        productStructureTypePostNewData:@json(route('mgr.productStructureType.storeNewData')),
                        productStructureTypeDeleteData:@json(route('mgr.productStructureType.deleteData'))
                    },
                    apiLoading: {
                        productTypeAutoComplete: true,
                        materialTypeAutoComplete: true,
                        nodeParentAutoComplete: true,
                        materialUnitTypeAutoComplete: true,
                        productStructureTypeTable: true,
                    },
                    apiData: {
                        autoComplete: {
                            productType: [],
                            materialType: [],
                            nodeParent: [],
                            materialUnitType: [],
                        },
                        table: {
                            productStructureType: {
                                headers: [
                                    {text: 'ID Node', value: 'id'},
                                    {text: 'Nama', value: 'name'},
                                    {text: 'Tipe Produk', value: 'productType'},
                                    {text: 'Tipe Material', value: 'materialType'},
                                    {text: 'Node Parent', value: 'parentNode'},
                                    {text: 'Jml', value: 'total'},
                                    {text: 'Satuan', value: 'units'},
                                    {text: 'actions', value: 'actions', width: 200}
                                ],
                                listing: [],
                            }
                        }
                    },
                    form: {
                        idNode: null,
                        productType: "0000",
                        materialType: "000000000000",
                        nodeParent: null,
                        name: null,
                        total: null,
                        unitType: 'AA'
                    },
                    formClientError: {},
                    formServerError: {}
                }
            },
            async mounted() {
                await this.callProductTypeAutoCompleteData();
                await this.callMaterialTypeAutoCompleteData();
                await this.callProductTypeStructureAutoCompleteData();
                await this.callMaterialUnitTypeAutoCompleteData();
                await this.callProductStructureTypeTableData();
            },
            watch: {
                'form': function (v) {
                    console.log(v)
                }
            },
            methods: {
                async callProductTypeAutoCompleteData() {
                    await axios.get(this.urlData.productTypeAutoCompleteData).then((r) => {
                        this.apiData.autoComplete.productType = r.data;
                    }).finally(() => {
                        this.apiLoading.productTypeAutoComplete = false;
                    })
                },
                async callMaterialTypeAutoCompleteData() {
                    await axios.get(this.urlData.materialTypeAutoCompleteData).then((r) => {
                        this.apiData.autoComplete.materialType = r.data;
                    }).finally(() => {
                        this.apiLoading.materialTypeAutoComplete = false;
                    })
                },
                async callProductTypeStructureAutoCompleteData() {
                    await axios.get(this.urlData.productStructureTypeAutoCompleteData).then((r) => {
                        this.apiData.autoComplete.nodeParent = r.data;
                    }).finally(() => {
                        this.apiLoading.nodeParentAutoComplete = false;
                    })
                },
                async callMaterialUnitTypeAutoCompleteData() {
                    await axios.get(this.urlData.materialUnitTypeAutoCompleteData).then((r) => {
                        this.apiData.autoComplete.materialUnitType = r.data;
                    }).finally(() => {
                        this.apiLoading.materialUnitTypeAutoComplete = false;
                    })
                },
                async callProductStructureTypeTableData() {
                    this.apiLoading.productStructureTypeTable = true;
                    await axios.get(this.urlData.productStructureTypeTableData).then((r) => {
                        this.apiData.table.productStructureType.listing = r.data;
                    }).finally(() => {
                        this.apiLoading.productStructureTypeTable = false;
                    })
                },
                async postNewProductStructureType() {
                    await axios.post(this.urlData.productStructureTypePostNewData, {...this.form})
                        .then((r) => {
                            this.formClientError = {}
                            this.callProductStructureTypeTableData();
                        }).catch((e) => {
                            this.controllerFormServerError(e.response.data);
                        })
                },
                async commitDeleteProductStructureType() {
                    axios.delete(this.urlData.productStructureTypeDeleteData, {params: this.selectedToDelete})
                        .then((r) => {
                            this.selectedToDelete = {};
                            this.snackbar.isDeleteCautionSnackbarOpened = false;
                            this.callProductStructureTypeTableData();
                        })
                        .catch()
                        .finally()
                },
                async cancelDeleteProductStructureType() {
                    this.selectedToDelete = {};
                    this.snackbar.isDeleteCautionSnackbarOpened = false;
                },
                async deleteProductStructureType(data) {
                    console.log(data)
                    this.selectedToDelete = data;
                    this.snackbar.isDeleteCautionSnackbarOpened = true;
                },
                controllerFormServerError(data) {
                    let construction = {};
                    $.each(data.errors, (db, val) => {
                        construction[db] = val.toString();
                    })
                    this.formClientError = construction;
                },
                fieldCheckerIdNode() {

                }
            }
        };
        window.pageMix.push(lAMixingPage);
    </script>
@endpush
